# ilk

## Overview
**ilk** is a personal project, currently in-development, with the ambition to become the best python scaffolding project around.

It is an extremely opinionated, painstakingly considered, obsessively diligent repository template for a python project (library or application). To see if it's right for you, check if you agree with the [opinions](#opinions).

Ilk's goal is to provide a well-maintained python project template with all the [functionality](#features) you should use and none that you should not. Whilst there is sufficient flexibility so that irrelevant functionality is excluded, e.g. no Dockerfile when creating a library, in general, ilk deliberately adopts a one-size-fits-all approach in order to force you to:
* mature your development workflow around modern best practices
* utilise the best 3rd party tools
* maximise your productivity in producing a high quality python project

All of the 3rd party tools that provide various capabilities for ilk, have been selected as best-in-their-class - arrived at through careful consideration of:
* their current capabilities
* direction of travel
* how well maintained they are

The hope is that projects based on ilk will follow in **i**ts **l**i**k**eness and be the best _insert purpose here_ project around.

## Setup
### System Dependencies
You will need the following:
* [Git](https://git-scm.com/downloads) - with a gitlab profile configured with an ssh key if you want to use `invoke init` to manage the remote repository
* [Python](https://www.python.org/downloads/)
* [Docker](https://docs.docker.com/get-docker/) - only needed if you are generating an application

### Installation Pre-Requisites
* Install [pipx](https://pypa.github.io/pipx) user-wide if not already installed:
```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath
```

* Install [copier](https://copier.readthedocs.io/en/stable/) user-wide (and inject dependencies) if not already installed:
```bash
pipx install copier
pipx inject copier copier-templates-extensions
```

## Usage
### Create New Project
1. Initialise a new project in your current directory using copier, based off ilk:
```bash
copier gl:adamcunnington/ilk .
```
_Note: if you have not already created your project directory, replace `.` with your desired directory name._

2. Initialise the git repo locally/remotely/link to existing by running the following command after activating your virtual environment:
```bash
invoke init
```

3. Add code to the python package directory and the tests directory - at minimum, add a docstring to `__init__.py`.

4. Customise project metadata in `pyproject.toml`.

5. Customise .gitlab-ci.yml according to your needs - in particular, populate the deployment jobs and configure variables in GitLab CI environments accordingly.

### Update Existing Project
Integrate the latest changes from ilk by running the following command after activating your virtual environment:
```bash
invoke update
```

### Run Commands
Activate your project's virtual environment and see the list of available invoke commands:
```bash
invoke --list
```

**Top Tip:** Set an alias to activate your current directory's virtual environment to save keystrokes:
```bash
echo "alias act=\". .venv/bin/activate\"" >> .bash_aliases
```
_Note: This is only for POSIX platforms,_

## Features
Broadly speaking, ilk has the following features:
* A sensible way of creating a new project/integrating template changes into an existing project - via [pipx](https://pypa.github.io/pipx) and [copier](https://copier.readthedocs.io/en/stable/).
* A single command line interface for interacting with all developer tooling - via [invoke](https://www.pyinvoke.org/).
* Fully ready GitLab CICD pipeline configuration ...

The ilk template uses the following 3rd party python projects by category:



## Opinions
The following opinions underpin the design decisions:


## Known Limitations
The following limitations are known. They are not critical (i.e. there is either a workaround or the effect is an impairment of "quality of life" rather than functionality) but the intention is to optimise them away:
* Updating a python project based on ilk takes a while because the freeze & install tasks are slow - ideally, these tasks would be optional by default - we are dependent on an enhancement to copier for this.
