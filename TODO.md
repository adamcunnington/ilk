# To Do

## Ilk
[x] Delete .editorconfig and CHANGELOG.md whilst dev workflow for Ilk itself, is undecided
[ ] Update README so it is accurate and well-written based on initial tool set
[ ] Remove the is_windows functionality - linux-only is an opinion we're sticking to
[ ] Review copier questions and check happy with names/descriptions, and whether any redundant/missing questions
[ ] Decide on the dev workflow for Ilk itself - document in CONTRIBUTING.md
[ ] Based on the above, create/populate CHANGELOG.md and other files (.editorconfig, LICENSE etc.)
[ ] Add a section to README.md about local dev environment (editor setup & extensions etc., WSL2)
[ ] Figure out how to test Ilk itself and implement

## Project Template - OOB Functionality
[ ] Add is_application condition to requirements.txt file and consider impact on install task; leave comment
[ ] Close out open questions
    [ ] Should we automatically generate changelog - how to reduce brittleness
    [ ] How sophisticated should commit task be - push & set upstream / generate message independently to use with git directly?
    [ ] More generally, what other git operations should be abstracted? squash? merge-request? cicd namespace?
    [ ] How to separate tasks we don't expect to be changed vs. user-managed tasks - how do users override?
    [ ] How to make it really clear which bits a user should edit when creating template - e.g. stuff in pyproject.toml
    [ ] Review conclusions around lint tooling and store somewhere
    [ ] How should environment variables be loaded? Maybe also account for environment specific variables in order to replicate gitlab CI environments. E.g. .dev.env. Maybe there should be a .env dir with .vars and .<env>.vars files inside?
    [ ] See if _SOURCE_CODE_PATHS can instead be replaced by a global that excludes all hidden files (or at least .venv)
[ ] Populate tasks.py, docstrings & thoroughly test
    [ ] Move some tasks behind is_application check such as docker/run/deploy stuff - think from library perspective
    [ ] Go through every task and complete/consider limitations and add to dos below
        [ ] clean: add parameter to clean up docker artifacts (images/containers)
        [ ] init: support  various params (e.g. `git_init=True`, `create_repo=True`,  `existing_repo=None`) which would be responsible for local and/or remote repo creation/linkage.
    [ ] Review limitation around slow template update
    [ ] Parallelise slow context.run() operations such as with the freeze task
    [ ] Review scenarios where context.run(warn=True) would be useful to avoid failure - but how to handle this?
[ ] Populate simple stub files:
    [ ] .dockerignore
    [ ] .editorconfig
    [x] .gitignore
    [x] {{ _copier_conf.answers_file }}
    [x] LICENSE
    [x] requirements.txt
[ ] Populate pyproject.toml
[ ] Implement .mkdocs.yml
[ ] Implement .pre-commit-config
[ ] Implement Dockerfile
[ ] Implement CI
    [ ] Populate .gitlab-ci.yml
    [ ] Extend README with comments about CI and configuration - move configuration notes into common section?
    [ ] Consider whether .env file should be added - consider how this gets committed/updated despite gitignored
[ ] Populate CONTRIBUTING.md
[ ] Populate README.md

## Project Template - Future Functionality
[ ] Review additional tooling and decide whether to include
    [ ] Gitpod / Codespaces