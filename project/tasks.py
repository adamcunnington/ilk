"""Define invoke tasks to unify the interface to developer tooling."""

import os
from collections.abc import Iterable
from typing import Generator

import invoke
import toml

_ENV_VAR_GITLAB_CI_DEPLOY_PASSWORD = "CI_DEPLOY_PASSWORD"
_ENV_VAR_GITLAB_CI_DEPLOY_USER = "CI_DEPLOY_USER"
_ENV_VAR_GITLAB_CI_REGISTRY = "CI_REGISTRY"
_ENV_VAR_GITLAB_CI_REGISTRY_IMAGE = "CI_REGISTRY_IMAGE"
_ENV_VAR_GITLAB_REGISTRY_IDS = "GITLAB_REGISTRY_IDS"  # space-delimited string
_EXTRA_REQUIREMENTS_DIR_NAME = "extra-requirements"
_GITLAB_PYPI_URL_TEMPLATE = "https://{user}:{password}@gitlab.com/api/v4/projects/{project_id}/packages/pypi/simple"

_this_dir_path = os.path.dirname(__file__)
_this_dir = os.path.basename(_this_dir_path)

namespace = invoke.Collection()


def _docker_login(context: invoke.Context) -> None:
    options = [
        f"--password={os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_PASSWORD]}",
        f"--username={os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_USER]}"
    ]
    context.run(f"docker login {' '.join(options)} {os.environ[_ENV_VAR_GITLAB_CI_REGISTRY]}")


def _get_version(context: invoke.Context) -> str:
    return version_get(context, hide=True).stdout.strip()


def _prepare_docker_tag(context: invoke.Context) -> str:
    return f"{os.environ[_ENV_VAR_GITLAB_CI_REGISTRY_IMAGE]}:{_get_version(context)}"


def _prepare_extra_index_urls() -> Generator[str, None, None]:
    gitlab_registry_ids = os.environ[_ENV_VAR_GITLAB_REGISTRY_IDS]
    if gitlab_registry_ids:
        user = os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_USER]
        password = os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_PASSWORD]
        for gitlab_project_id in gitlab_registry_ids.split():
            extra_index_url = _GITLAB_PYPI_URL_TEMPLATE.format(
                user=user, password=password, gitlab_project_id=gitlab_project_id
            )
            yield f"--extra-index-url={extra_index_url}"


app = invoke.Collection("app")
namespace.add_collection(app)


@invoke.task()
def app_deploy(_context: invoke.Context) -> None:
    raise NotImplementedError()


app.add_task(app_deploy, "deploy")


@invoke.task()
def app_run(_context: invoke.Context) -> None:
    raise NotImplementedError()


app.add_task(app_run, "run")


@invoke.task()
def clean(context: invoke.Context) -> None:
    """Recursively delete temporary/cache directories and files."""
    directories = ("*.egg-info", ".mypy_cache", ".pytest_cache", "__pycache__", "build", "dist", "public")
    files = ("*.py[cdo]", ".coverage")
    context.run(rf"find . -type f \( -iname {' -o -iname '.join(files)} \) -delete", echo=True)
    context.run(
        rf"find . -type d \( -iname {' -o -iname '.join(directories)} \) -print0 | xargs -0 --no-run-if-empty rm -rf",
        echo=True,
    )


namespace.add_task(clean)


@invoke.task()
def commit(context: invoke.Context) -> None:
    """Generate a compliant conventional commit message interactively via commitizen"s prompts."""
    context.run("cz commit", echo=True)


namespace.add_task(commit)
{%- if is_application %}

docker = invoke.Collection("docker")
namespace.add_collection(docker)


@invoke.task()
def docker_build(context: invoke.Context) -> None:
    """Build a docker image, tagged with the current version, ready for the remote private registry."""
    options = [
        f"--build_arg=CI_DEPLOY_PASSWORD={os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_PASSWORD]}",
        f"--build-arg=CI_DEPLOY_USER={os.environ[_ENV_VAR_GITLAB_CI_DEPLOY_USER]}",
        "--pull",
        f"--tag={_prepare_docker_tag()}"
    ]
    context.run(f"docker build {' '.join(options)} .", echo=True)


docker.add_task(docker_build, "build")


@invoke.task()
def docker_pull(context: invoke.Context) -> None:
    """Pull the docker image, tagged with the current version, from the remote private registry."""
    _docker_login(context)
    context.run(f"docker pull {_prepare_docker_tag}")


docker.add_task(docker_pull, "pull")


@invoke.task()
def docker_push(context: invoke.Context) -> None:
    """Push the docker image, tagged with the current version, to the remote private registry."""
    _docker_login(context)
    context.run(f"docker push {_prepare_docker_tag}")


docker.add_task(docker_push, "push")


@invoke.task()
def docker_run(_context: invoke.Context) -> None:
    raise NotImplementedError()


docker.add_task(docker_run, "run")
{% endif -%}

docs = invoke.Collection("docs")
namespace.add_collection(docs)


@invoke.task()
def docs_build(context: invoke.Context) -> None:
    """Build the documentation site static files."""
    context.run("mkdocs build --config-file=.mkdocs.yml")


docs.add_task(docs_build, "build")


@invoke.task()
def docs_serve(context: invoke.Context) -> None:
    """Run the documentation development web server."""
    context.run("mkdocs serve --config-file=.mkdocs.yml")


docs.add_task(docs_serve, "serve")


@invoke.task(help={"check": "View proposed changes without applying them"})
def format_(context: invoke.Context, check: bool = False) -> None:
    """Run code formatting tools (isort, black)."""
    options = " --diff" if check else ""
    context.run(f"isort{options} {_this_dir_path}", echo=True)
    context.run(f"black{options} {_this_dir_path}", echo=True)


namespace.add_task(format_, "format")


@invoke.task(
    iterable=("extras",),
    help={
        "upgrade": "Forcibly upgrade frozen requirements to the latest versions that are compatible (default = False)",
        "deps": "Whether to update main requirements (default = True)",
        "extra_deps": "Whether to update extra requirements (default = True)",
        "extras": "[repeatable] The specific extra requirement groups to update (default = None which means all)",
    },
)
def freeze(
    context: invoke.Context,
    upgrade: bool = False,
    deps: bool = True,
    extra_deps: bool = True,
    extras: Iterable[str] = (),
) -> None:
    """Create or update requirement files."""
    pip_compile_args = [
        "pip-compile",
        "pyproject.toml",
        "--allow-unsafe",
        "--no-emit-index-url",
        "--quiet",
        "--resolver=backtracking",
    ]
    if upgrade:
        pip_compile_args.append("--upgrade")
    pip_compile_args.extend(_prepare_extra_index_urls())
    if deps:
        context.run(" ".join(pip_compile_args), echo=True)
    if extra_deps:
        if not extras:
            with open("pyproject.toml", encoding="utf8") as pyproject_toml:
                extras = toml.load(pyproject_toml)["project"]["optional-dependencies"].keys()
        for extra in extras:
            if extra == "install":
                continue
            output_file = os.path.join(_EXTRA_REQUIREMENTS_DIR_NAME, f"{extra}-requirements.txt")
            context.run(f"{' '.join(pip_compile_args)} --extra={extra} --output-file={output_file}", echo=True)


namespace.add_task(freeze)


@invoke.task(
    help={
        "existing_repo_uri": "The existing repo's git URI to add as a SSH remote, called origin, to the local repo",
        "gitlab_namespace_id": "If remote_repo is provided, the repo's parent namespace (defaults to user's)",
        "local_repo": "Whether to attepmt to initialise a local git repo at the project root",
        "remote_repo": "Whether to attempt to create a remote GitLab repo (named after the local repo's directory name)"
    },
)
def init(
    context: invoke.Context, 
    existing_repo_uri: str = "", 
    gitlab_namespace_id: str = "", 
    local_repo: bool = True, 
    remote_repo: bool = True
) -> None:
    """Initialise git repositories - just locally, just remotely, both, or link existing remote to local."""
    if local_repo:
        context.run(f"git init {_this_dir_path}", echo=True)
    if remote_repo:
        # make request
        # existing_repo_uri = ""
        pass
    if existing_repo_uri:
        context.run(f"git remote add origin {existing_repo_uri}", echo=True)
        


namespace.add_task(init)


@invoke.task(
    iterable=("extras",),
    help={
        "extra_deps": "Whether to install extra requirements (update = True)",
        "extras": "[repeatable] The specific extra requirement groups to install (default = dev)",
    },
)
def install(context: invoke.Context, extra_deps: bool = True, extras: Iterable[str] = ("dev",)) -> None:
    """Synchronise installed dependencies and install self."""
    requirements = (
        "requirements.txt"
        if not extra_deps
        else " ".join(f"{_EXTRA_REQUIREMENTS_DIR_NAME}/{extra}-requirements.txt" for extra in extras)
    )
    pip_sync_args = ["pip-sync", requirements]
    pip_sync_args.extend(_prepare_extra_index_urls())
    context.run(" ".join(pip_sync_args), echo=True)
    context.run(f"pip install{' --editable=' if extra_deps else ''}.", echo=True)


namespace.add_task(install)


@invoke.task()
def lint(context: invoke.Context) -> None:
    context.run(f"bandit --configfile=pyproject.toml --recursive {_this_dir_path}", echo=True)
    context.run(f"mypy {_this_dir_path}", echo=True)
    context.run(f"pflake8 {_this_dir_path}", echo=True)
    context.run(f"pydocstyle {_this_dir_path}", echo=True)
    context.run(f"pylint {_this_dir_path}", echo=True)
    context.run(f"safety check --file=requirements.txt", echo=True)
    context.run(f"vulture {_this_dir_path}", echo=True)


namespace.add_task(lint)

test = invoke.Collection("test")
namespace.add_collection(test)


@invoke.task()
def test_unit(_context: invoke.Context) -> None:
    pass


test.add_task(test_unit, "unit")


@invoke.task()
def update(context: invoke.Context) -> None:
    context.run("copier update", echo=True)


namespace.add_task(update)

version = invoke.Collection("version")
namespace.add_collection(version)


@invoke.task()
def version_bump(_context: invoke.Context) -> None:
    pass


version.add_task(version_bump, "bump")


@invoke.task()
def version_get(context: invoke.Context, hide: bool = False) -> str:
    """Output the project"s current version."""
    return context.run("cz version --project", echo=True, hide=hide)


version.add_task(version_get, "get")

wheel = invoke.Collection("wheel")
namespace.add_collection(wheel)


@invoke.task()
def wheel_build(context: invoke.Context) -> None:
    context.run("build", echo=True)


wheel.add_task(wheel_build, "build")


@invoke.task()
def wheel_download(_context: invoke.Context) -> None:
    context.run("pip download", echo=True)


wheel.add_task(wheel_download, "download")


@invoke.task()
def wheel_publish(context: invoke.Context) -> None:
    context.run("flit publish", echo=True)


wheel.add_task(wheel_publish, "publish")
