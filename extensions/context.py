import os

import copier_templates_extensions


_WINDOWS_OS_NAME = "nt"


class ContextUpdater(copier_templates_extensions.ContextHook):
    def hook(self, _context: dict) -> dict:
        return {
            "is_windows": os.name == _WINDOWS_OS_NAME
        }
